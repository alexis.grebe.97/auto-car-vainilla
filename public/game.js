const canvas = document.getElementById('gameCanvas');
        const ctx = canvas.getContext('2d');

        const carWidth = 40;
        const carHeight = 80;
        let carX = canvas.width / 2 - carWidth / 2;
        let carY = canvas.height - carHeight - 20;
        let carSpeed = 1; // Cambiar la velocidad a 1 píxel por fotograma
        let score = 0;
        let gameOver = false;

        const carImage = new Image();
        carImage.src = 'car.png';

        const enemyCarImage = new Image();
        enemyCarImage.src = 'enemy_car.png';

        let enemyCars = [];

        function drawRoad() {
            ctx.beginPath();
            ctx.strokeStyle = "#fff";
            ctx.lineWidth = 10;
            ctx.moveTo(canvas.width / 3, 0);
            ctx.lineTo(canvas.width / 3, canvas.height);
            ctx.moveTo(canvas.width * 2 / 3, 0);
            ctx.lineTo(canvas.width * 2 / 3, canvas.height);
            ctx.stroke();
        }

        function drawCar() {
            ctx.drawImage(carImage, carX, carY, carWidth, carHeight);
        }

        function drawEnemyCars() {
            for (let i = 0; i < enemyCars.length; i++) {
                const enemyCar = enemyCars[i];
                ctx.drawImage(enemyCarImage, enemyCar.x, enemyCar.y, carWidth, carHeight);
            }
        }


        function moveCarWithMouse(event) {
            const rect = canvas.getBoundingClientRect();
            carX = event.clientX - rect.left - carWidth / 2;
            // Asegurarse de que el auto no salga del límite izquierdo del canvas
            if (carX < 0) {
                carX = 0;
            }
            // Asegurarse de que el auto no salga del límite derecho del canvas
            if (carX > canvas.width - carWidth) {
                carX = canvas.width - carWidth;
            }
        }


        function moveCar(event) {
            if (event.touches.length > 0) {
                const rect = canvas.getBoundingClientRect();
                carX = event.touches[0].clientX - rect.left - carWidth / 2;
            }
            if (carX < 0) {
                carX = 0;
            }
            // Asegurarse de que el auto no salga del límite derecho del canvas
            if (carX > canvas.width - carWidth) {
                carX = canvas.width - carWidth;
            }
        }
        
        canvas.addEventListener('touchmove', moveCar);
        

        canvas.addEventListener('mousemove', moveCarWithMouse);

        function generateEnemyCar() {
            const randomX = Math.random() * (canvas.width - carWidth);
            const enemyCar = {
                x: randomX,
                y: -carHeight,
                speed: Math.random() * 4 + 2 // Random speed between 2 and 6
            };
            enemyCars.push(enemyCar);
        }

        function moveEnemyCars() {
            for (let i = 0; i < enemyCars.length; i++) {
                const enemyCar = enemyCars[i];
                enemyCar.y += enemyCar.speed;
                if (enemyCar.y > canvas.height) {
                    enemyCars.splice(i, 1);
                    score++;
                }
                if (checkCollision(enemyCar)) {
                    gameOver = true;
                }
            }
        }

        function checkCollision(enemyCar) {
            return (
                carX < enemyCar.x + carWidth &&
                carX + carWidth > enemyCar.x &&
                carY < enemyCar.y + carHeight &&
                carY + carHeight > enemyCar.y
            );
        }

        function updateGame() {
            if (!gameOver) {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                drawRoad();
                drawCar();
                drawEnemyCars();
                moveEnemyCars();
                requestAnimationFrame(updateGame);
            } else {
                ctx.fillStyle = '#ffffff';
                ctx.font = '30px Arial';
                ctx.fillText('Game Over', canvas.width / 2 - 80, canvas.height / 2);
                ctx.fillText('Score: ' + score, canvas.width / 2 - 60, canvas.height / 2 + 40);
            }
        }

        setInterval(generateEnemyCar, 350);

        updateGame();